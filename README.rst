Open Backscatter Toolchain
==========================

.. image:: https://github.com/hydroffice/hyo2_openbst/raw/master/hyo2/openbst/app/media/favicon.png
    :alt: logo

|

.. image:: https://img.shields.io/badge/docs-latest-brightgreen.svg
    :target: https://www.hydroffice.org/manuals/openbst/index.html
    :alt: Latest Documentation

.. image:: https://api.codacy.com/project/badge/Grade/a5f19e6e289246daa95ccb4d47aca527
    :target: https://www.codacy.com/app/hydroffice/hyo2_openbst/dashboard
    :alt: codacy

|

* GitHub: `https://github.com/hydroffice/hyo2_openbst <https://github.com/hydroffice/hyo2_openbst>`_
* Project page: `url <https://www.hydroffice.org/openbst/main>`_
* Download page: `url <https://bitbucket.org/hydroffice/hyo_openbst/downloads/>`_
* License: LGPLv3 or IA license (See `Dual license <https://www.hydroffice.org/license/>`_)

|

General info
------------

The Open Basckscatter Toolchain (OBST) package is a library and an app for processing acoustic backscatter.

|

Credits
-------

Authors
~~~~~~~

This code is written and maintained by:

- `Giuseppe Masetti <mailto:gmasetti@ccom.unh.edu>`_
- `Jean-Marie Augustin <mailto:jean.marie.augustin@ifremer.fr>`_


Contributors
~~~~~~~~~~~~

The following wonderful people contributed directly or indirectly to this project:

- `John Doe <mailto:john.doe@email.me>`_

Please add yourself here alphabetically when you submit your first pull request.
